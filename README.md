# OpenML dataset: fifa

https://www.openml.org/d/44032

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Dataset used in the tabular data benchmark https://github.com/LeoGrin/tabular-benchmark, transformed in the same way. This dataset belongs to the "regression on numerical features" benchmark.
        Original source: https://www.kaggle.com/datasets/stefanoleone992/fifa-22-complete-player-dataset?select=players_22.csv Please give credit to the original source if you use this dataset.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44032) of an [OpenML dataset](https://www.openml.org/d/44032). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44032/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44032/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44032/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

